# runit

UNIX init scheme with service supervision. https://en.m.wikipedia.org/wiki/Runit

# Unofficial documentation
* [*Runit*](https://wiki.archlinux.org/index.php/Runit)

# Runit on Fedora
## Using Busybox
* [*runit is not packaged on fedora.*
  ](https://gitlab.com/gitlab-org/gitlab-development-kit/issues/496)
  (2019)
* [*Runit*](https://wiki.archlinux.org/index.php/Runit)

## Contributed Runit RPM
* RPM Sphere
  * https://repology.org/project/runit/versions